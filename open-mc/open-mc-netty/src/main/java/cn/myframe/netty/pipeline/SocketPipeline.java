package cn.myframe.netty.pipeline;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import cn.myframe.netty.handle.SocketServerHandler;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @author ynz
 * @version 创建时间：2018/6/26
 * @email ynz@myframe.cn
 */
@Component
@ConditionalOnProperty(  //配置文件属性是否为true
        value = {"netty.socket.enabled"},
        matchIfMissing = false
)
public class SocketPipeline extends ChannelInitializer<SocketChannel>{
	
	@Autowired
	SocketServerHandler socketServerHandler;
	
	@Override
	public void initChannel(SocketChannel ch) throws Exception {

		ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast(new StringEncoder());
		pipeline.addLast(new StringDecoder());
		pipeline.addLast(new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer("\r\n\r\n".getBytes())));
		pipeline.addLast(socketServerHandler);
		
	}

}
